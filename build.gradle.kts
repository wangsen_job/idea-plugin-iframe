plugins {
    id("java")
    id("org.jetbrains.intellij") version "1.12.0"
//    id("org.jetbrains.intellij") version "1.16.1"
}

group = "com.luomacode"
version = "7.1.1"

repositories {
    maven { setUrl("https://maven.aliyun.com/nexus/content/groups/public/") }
//    mavenCentral()
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
//    version.set("2022.1.4")
    version.set("2023.2.6")
    type.set("IC") // Target IDE Platform

    plugins.set(listOf(/* Plugin Dependencies */))
}

tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "11"
        targetCompatibility = "11"
    }

    patchPluginXml {
        sinceBuild.set("221.*")
        untilBuild.set("291.*")
        changeNotes.set("""
            v7.1.0<br>
            2024/11/10<br>
            功能迭代：功能支持调用插件<br>
            功能迭代：支持创建自己的助手<br>
            体验优化：编程类助手增加icon<br>
            体验优化：多轮对话性能提高5倍<br>
            <img width="500px" src="https://luomacode-1253302184.cos.ap-beijing.myqcloud.com/chatmoss/v7.1.0.png" /><br>
            v7.0.0（大版本更新）<br>
            2024/11/3<br>
            功能迭代：更精简的首页，回答区域增加20%，首页按钮减少52%<br>
            功能迭代：艾特@助手功能，首期上线40+助手，使用助手解答问题效果提升70%<br>
            体验优化：右键拓展功能速度提高10倍（优化，解释代码等功能）<br>
            功能迭代：右侧面板功能，现在可以在文件中右键打开右侧面板，交互宽度提高45%<br>
            功能迭代：历史记录支持收藏功能了<br>
            数值修改：高级模型消耗次数降低，将原有3次，2次，统一改为1次<br>
            <img width="500px" src="https://luomacode-1253302184.cos.ap-beijing.myqcloud.com/chatmoss/v7.0.0.png" /><br>
        """.trimIndent())
    }

//    signPlugin {
//        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
//        privateKey.set(System.getenv("PRIVATE_KEY"))
//        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
//    }
//
//    publishPlugin {
//        token.set(System.getenv("PUBLISH_TOKEN"))
//    }
}
