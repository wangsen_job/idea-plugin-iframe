package com.luomacode.codemoss;


import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.jcef.JBCefBrowser;
import com.intellij.util.ui.JBUI;
import com.luomacode.codemoss.actions.shortcutkey.ShortCutKeyHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static com.luomacode.codemoss.GlobalWindowElementContext.*;

/**
 * web加载器
 *
 * @author wangsen
 * @since 2023/3/13 20:17
 */
public class MyWebToolWindowContent {

    private final JPanel content;

    /**
     * 构造函数
     */
    public MyWebToolWindowContent() {
        this.content = new JPanel(new BorderLayout());
        // 创建 JBCefBrowser
        // 将 JBCefBrowser 的UI控件设置到Panel中
//        THE_JB_CEF_BROWSER = JBCefBrowser.createBuilder().setOffScreenRendering(false).setEnableOpenDevToolsMenuItem(true).build();
        THE_JB_CEF_BROWSER = JBCefBrowser.createBuilder().setOffScreenRendering(false).build();
        // 将 JBCefBrowser 的UI控件设置到Panel中
        this.content.add(THE_JB_CEF_BROWSER.getComponent(), BorderLayout.CENTER);

        // 加载页面
//        THE_JB_CEF_BROWSER.loadURL("http://ip.aihao123.cn/index.html?hiddenInput=1#/chat");
        THE_JB_CEF_BROWSER.loadURL("https://pc.aihao123.cn/index.html?source=jetbrains#/chat");
//        THE_JB_CEF_BROWSER.loadURL("https://testcodemoss.aihao123.cn/#/chat");

        // 在底部添加输入框
        textArea = new JTextArea();
        // 设置自动换行
        textArea.setLineWrap(true);
        // 设置断行不断字
        textArea.setWrapStyleWord(true);
        textArea.setMargin(JBUI.insets(5));

        // 创建滚动面板，并添加文本区域
        JBScrollPane scrollPane = new JBScrollPane(textArea);

        // 把输入框隐藏起来，也就是仅保留其功能，不显示出来
        scrollPane.setVisible(false);
        // 设置滚动面板的首选大小
        scrollPane.setPreferredSize(new Dimension(scrollPane.getPreferredSize().width, 70));

        // 添加滚动面板到容器中（例如JFrame或JPanel）
        this.content.add(scrollPane, BorderLayout.SOUTH);
        THE_TEXT_AREA = textArea;

        // 初始化弹出菜单
        popupMenu = new JPopupMenu();
        for (String option : options) {
            JMenuItem menuItem = new JMenuItem(option);
            menuItem.addActionListener(e -> {
                // 当选中某个选项时，将其插入到文本区域中
                THE_TEXT_AREA.setText(THE_TEXT_AREA.getText() + option);
                popupMenu.setVisible(false);
            });
            popupMenu.add(menuItem);
        }


        // 给输入框添加回车事件
        // 添加回车事件
        textArea.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                ShortCutKeyHandler.doHandle(e);
            }
        });

        // 设置默认的提示文字
        textArea.setText(PLACE_HOLDER);

        // 添加焦点监听器
        textArea.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (textArea.getText().equals(PLACE_HOLDER)) {
                    textArea.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textArea.getText().isEmpty()) {
                    textArea.setText(PLACE_HOLDER);
                }
            }
        });
    }

    public static void doSendAsk() {
        String text = THE_TEXT_AREA.getText().replace("\n", "\\n").replace("'", "\\'");
        // 执行JS代码
//        THE_JB_CEF_BROWSER.getCefBrowser().executeJavaScript("onConversation('" + text + "')", "", 0);
        THE_JB_CEF_BROWSER.getCefBrowser().executeJavaScript("handleVscodeMessage('" + text + "')", "", 0);
        // 清空输入框
        THE_TEXT_AREA.setText("");
    }

    /**
     * 返回创建的JPanel
     * @return JPanel
     */
    public JPanel getContent() {
        return content;
    }
}