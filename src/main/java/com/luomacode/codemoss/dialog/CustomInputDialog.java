package com.luomacode.codemoss.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

/**
 * 可以自定义宽高的dialog
 *
 * @author wangsen
 * @since 2023/12/22 16:27
 */
public class CustomInputDialog extends DialogWrapper {
    private JPanel panel;
    private JTextField textField;
    private final String title;

    public CustomInputDialog(@Nullable Project project, String title) {
        super(project);
        this.title = title;
        init();
        setTitle(title);
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        panel = new JPanel(new BorderLayout());
        JLabel label = new JLabel("请输入内容:");
        panel.add(label, BorderLayout.NORTH);

        textField = new JTextField();
        panel.add(textField, BorderLayout.CENTER);

        // 设置对话框的首选大小
        panel.setPreferredSize(new Dimension(400, 100)); // 根据需要调整宽度和高度

        return panel;
    }

    @Nullable
    @Override
    protected ValidationInfo doValidate() {
        String input = textField.getText();
        if (input == null || input.trim().isEmpty()) {
            return new ValidationInfo("内容不能为空");
        }
        return super.doValidate();
    }

    @Nullable
    @Override
    public JComponent getPreferredFocusedComponent() {
        return textField;
    }

    public String getInputString() {
        return textField.getText();
    }

    public static String showDialog(Project project, String title) {
        CustomInputDialog dialog = new CustomInputDialog(project, title);
        if (dialog.showAndGet()) {
            return dialog.getInputString();
        }
        return null;
    }
}
