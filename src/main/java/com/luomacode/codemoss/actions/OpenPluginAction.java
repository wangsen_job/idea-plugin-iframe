package com.luomacode.codemoss.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;

/**
 * 打开插件菜单选项
 *
 * @author wangsen
 * @since 2023/12/22 14:45
 */
public class OpenPluginAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent e) {
        // 获取当前编辑器
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (editor != null) {
            // 获取当前的项目上下文
            Project project = e.getProject();
            if (project == null) return;

            // 获取工具窗口管理器
            ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);

            // 获取你的工具窗口
            ToolWindow toolWindow = toolWindowManager.getToolWindow("CodeMoss");
            if (toolWindow != null) {
                // 切换 Tool Window 的可见性
                if (toolWindow.isVisible()) {
                    toolWindow.hide(null);
                } else {
                    toolWindow.show(null);
                }
            }
        }
    }
}
