package com.luomacode.codemoss.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.luomacode.codemoss.MyWebToolWindowContent;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;

import static com.luomacode.codemoss.GlobalWindowElementContext.THE_TEXT_AREA;

/**
 * 打开插件菜单选项并解释选中的代码
 *
 * @author wangsen
 * @since 2023/12/22 14:45
 */
public class OpenAndExplainPluginAction extends AnAction {

    private static final String SELECT_MSG = "请选中要解释的代码";

    @Override
    public void actionPerformed(AnActionEvent e) {
        // 加载自定义图标
        Icon customIcon = IconLoader.getIcon("/code_moss_icon.png", getClass());
        // 获取当前编辑器
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (editor != null) {
            // 获取选择模型
            SelectionModel selectionModel = editor.getSelectionModel();
            // 获取选中的文本
            String selectedText = selectionModel.getSelectedText();
            if (StringUtils.isBlank(selectedText)) {
                Messages.showMessageDialog(SELECT_MSG, SELECT_MSG, customIcon);
                return;
            }

//            System.out.println(String.format("选中的代码内容: %s", selectedText));

            // 获取当前的项目上下文
            Project project = e.getProject();
            if (project == null) return;

            // 获取工具窗口管理器
            ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);

            // 获取你的工具窗口
            ToolWindow toolWindow = toolWindowManager.getToolWindow("CodeMoss");
//            System.out.println("获取窗口工具 = " + toolWindow);
            if (toolWindow != null) {
                // 显示并激活工具窗口
                toolWindow.show(() -> {
                    if (THE_TEXT_AREA != null) {
                        final String format = String.format("解释这段代码 用中文回答: %s", selectedText);
                        THE_TEXT_AREA.setText(format);
                        MyWebToolWindowContent.doSendAsk();

//                        System.out.println(String.format("输入框中的内容: %s", THE_TEXT_AREA.getText()));
                        // 请求焦点
//                        final boolean b = THE_TEXT_AREA.requestFocusInWindow();
//                        if (b) {
//                            try {
//                                Thread.sleep(200);
//                            } catch (InterruptedException ignore) {
//                            }
//                            // 延迟自动发送
//                            MyWebToolWindowContent.doSendAsk();
//                        }
                    }
                });
            }
        }
    }
}
