package com.luomacode.codemoss.actions.shortcutkey;

import com.luomacode.codemoss.MyWebToolWindowContent;

import javax.swing.*;
import java.awt.event.KeyEvent;

import static com.luomacode.codemoss.GlobalWindowElementContext.*;

/**
 * 快捷键处理器
 *
 * @author wangsen
 * @since 2024/11/7 22:16
 */
public class ShortCutKeyHandler {

    public static void doHandle(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER && e.getExtendedKeyCode() == KeyEvent.VK_ENTER) {
            MyWebToolWindowContent.doSendAsk();
            // 阻止回车键默认的换行行为
            e.consume();
        }
        if (e.getKeyCode() == KeyEvent.VK_ENTER && e.getExtendedKeyCode() == 0) {
            // shift + 回车 = 换行
            THE_TEXT_AREA.setText(THE_TEXT_AREA.getText() + "\n");
            // 阻止回车键默认的换行行为
            e.consume();
        }
        // 监听@符
        if ((e.getKeyCode() == KeyEvent.VK_2 && e.isShiftDown()) || e.getKeyCode() == KeyEvent.VK_AT) {
            // 显示弹出菜单
            popupMenu.show(textArea, 0, -popupMenu.getPreferredSize().height);
            selectedIndex = 0; // 重置选中索引
            highlightMenuItem(selectedIndex);
        }
        if (popupMenu.isVisible()) {
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                selectedIndex = (selectedIndex - 1 + options.length) % options.length;
                highlightMenuItem(selectedIndex);
                e.consume();
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                selectedIndex = (selectedIndex + 1) % options.length;
                highlightMenuItem(selectedIndex);
                e.consume();
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                JMenuItem menuItem = (JMenuItem) popupMenu.getComponent(selectedIndex);
                menuItem.doClick();
                e.consume();
            }
        }
    }

    // 高亮选中的菜单项
    private static void highlightMenuItem(int index) {
        for (int i = 0; i < popupMenu.getComponentCount(); i++) {
            JMenuItem menuItem = (JMenuItem) popupMenu.getComponent(i);
            menuItem.setArmed(i == index);
        }
    }
}
