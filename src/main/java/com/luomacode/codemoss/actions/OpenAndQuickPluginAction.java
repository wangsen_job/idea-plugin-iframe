package com.luomacode.codemoss.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.luomacode.codemoss.MyWebToolWindowContent;
import com.luomacode.codemoss.dialog.CustomInputDialog;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;

import static com.luomacode.codemoss.GlobalWindowElementContext.*;

/**
 * 打开插件菜单选项并快速提问
 *
 * @author wangsen
 * @since 2023/12/22 14:45
 */
public class OpenAndQuickPluginAction extends AnAction {

    private static final String SELECT_MSG = "请选中要快速提问的代码";

    @Override
    public void actionPerformed(AnActionEvent e) {
        // 获取当前编辑器
        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (editor != null) {
            // 加载自定义图标
            Icon customIcon = IconLoader.getIcon("/code_moss_icon.png", getClass());
            // 获取选择模型
            SelectionModel selectionModel = editor.getSelectionModel();
            // 获取选中的文本
            String selectedText = selectionModel.getSelectedText();
            if (StringUtils.isBlank(selectedText)) {
                Messages.showMessageDialog(SELECT_MSG, SELECT_MSG, customIcon);
                return;
            }

            // 获取当前的项目上下文
            Project project = e.getProject();
            if (project == null) return;

            // 弹出输入框并获取用户输入
            String userInput = CustomInputDialog.showDialog(project, "输入的内容将与选中代码块拼接到一起发起提问");
            if (StringUtils.isBlank(userInput)) {
                return;
            }

            // 获取工具窗口管理器
            ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);

            // 获取你的工具窗口
            ToolWindow toolWindow = toolWindowManager.getToolWindow("CodeMoss");
            if (toolWindow != null) {
                // 显示并激活工具窗口
                toolWindow.show(() -> {
                    if (THE_TEXT_AREA != null) {
                        THE_TEXT_AREA.setText(userInput + "\n" + selectedText);
                        MyWebToolWindowContent.doSendAsk();
                        // 请求焦点
//                        final boolean b = THE_TEXT_AREA.requestFocusInWindow();
//                        if (b) {
//                            try {
//                                Thread.sleep(50);
//                            } catch (InterruptedException ignore) {
//                            }
//                            // 延迟自动发送
//                            MyWebToolWindowContent.doSendAsk();
//                        }
                    }
                });
            }
        }
    }
}
