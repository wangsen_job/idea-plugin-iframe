package com.luomacode.codemoss;

import com.intellij.ui.jcef.JBCefBrowser;

import javax.swing.*;

/**
 * 全局窗口元素上下文
 *
 * @author wangsen
 * @since 2024/11/7 22:20
 */
public class GlobalWindowElementContext {

    public static JTextArea THE_TEXT_AREA;
    public static JBCefBrowser THE_JB_CEF_BROWSER;
    public static JTextArea textArea;


    public static final String PLACE_HOLDER = "在此处输入，可以问我任何问题...";
    // 在类中定义一个JPopupMenu和选项列表
    public static JPopupMenu popupMenu;
    public static String[] options = {"张三", "李四", "王五"};
    public static int selectedIndex = -1; // 当前选中的索引

}
